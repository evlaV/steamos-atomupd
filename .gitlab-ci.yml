# Copyright 2021 Collabora Ltd
# SPDX-License-Identifier: MIT

workflow:
    rules:
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH

stages:
    - prepare
    - build
    - test

default:
  tags:
    - docker
    - linux

.test_dependencies: &test_dependencies
    - |
        set -eux

        apt-get -y update
        apt-get -y upgrade
        apt-get -y --no-install-recommends install \
        meson \
        python3 \
        python3-flask \
        python3-semantic-version \
        ${NULL+}

.test_template:
    stage: test
    needs: []
    tags:
        - docker
        - linux

test:debian-10:
    extends: .test_template
    image: debian:buster-slim
    script:
        - *test_dependencies
        - |
            set -eux

            meson _build/tests
            ninja -C _build/tests
            meson test --verbose -C _build/tests

    artifacts:
        paths:
            - _build/tests/meson-logs/*.txt
        when: always

test:debian-11:
    extends: .test_template
    image: debian:bullseye-slim
    script:
        - *test_dependencies
        - |
            set -eux

            meson _build/tests
            ninja -C _build/tests
            meson test --verbose -C _build/tests

    artifacts:
        paths:
            - _build/tests/meson-logs/*.txt
        when: always

# We can't do this in the docker step because the kaniko executor
# doesn't have git, therefore no `git describe`
prepare:
    stage: prepare
    image: debian:bullseye-slim
    script:
        - ci/prepare.sh > VERSION
    artifacts:
        paths:
            - VERSION

docker:
    stage: build
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    needs:
        - prepare
    script:
        - sh ci/docker.sh

# vim:set sw=4 sts=4 et:
